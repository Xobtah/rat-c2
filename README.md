# rat-c2 (Project under development)

A RAT and a C2 server that communicate over HTTPS. The C2 is able to send commands to the RAT and receive the output of the commands. The RAT is able to update itself. The C2 relies on an SQLite database to store the RATs pending jobs, results, and binaries.

Improvements to be made:
* Add signature verification to the RAT (XChaCha20-Poly1305)
* Check whether HTTPS is enough for safe communication or if manual encryption is necessary
* Add WASM runtime to the RAT
* Improve the RAT's ability to hide itself
* Improve the RAT's ability to update itself
* Add the ability to communicate over Tor
* Add the ability to communicate over an opened TCP session
* More...

## Build

> cargo build --release

## Usage

### C2

The C2 needs two directories to work:
* `certs`: Contains the certificate and the private key of the C2 server
* `db`: Contains the SQLite database

> ./c2

### RAT

> ./rat [C2 URL]
